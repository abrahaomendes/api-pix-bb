package br.com.gerentenet.apipixitau.apipixitau.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class Valor {
    private String original;
}
