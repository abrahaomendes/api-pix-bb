package br.com.gerentenet.apipixitau.apipixitau.service;

import br.com.gerentenet.apipixitau.apipixitau.model.CobInput;
import br.com.gerentenet.apipixitau.apipixitau.model.CobResponse;
import br.com.gerentenet.apipixitau.apipixitau.token.AuthTokenItau;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;


@Service
public class CobService {
    @Autowired
    private AuthTokenItau auth;

    @Autowired
    private CobUtilService util;

    private String url = "https://sandbox.devportal.itau.com.br/itau-ep9-gtw-pix-recebimentos-ext-v2/v2/cob";

    public Mono<CobResponse> geraCobPix(CobInput input){
        try {
            WebClient webClient = WebClient.create(url);
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            headers.add("Authorization", "Bearer "
                    + auth.getToken("de744fca-a606-3b07-92e0-712cf31c9346",
                    "88c79394-6b3f-426d-86a2-1e74c922aaa7"));
            headers.add("x-itau-apikey", util.criarApikey());

            return webClient
                    .post()
                    .uri(url)
                    .headers(httpHeaders -> httpHeaders.addAll(headers))
                    .bodyValue(input)
                    .retrieve()
                    .bodyToMono(CobResponse.class);
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }
    public Mono<CobResponse> geraCobImediata(String txId, CobInput input){
        try {
            WebClient webClient = WebClient.create(url);
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            headers.add("Authorization", "Bearer "
                    + auth.getToken("de744fca-a606-3b07-92e0-712cf31c9346",
                    "88c79394-6b3f-426d-86a2-1e74c922aaa7"));
            headers.add("x-itau-apikey", util.criarApikey());

            return webClient
                    .put()
                    .uri(url.concat("/"+txId))
                    .headers(httpHeaders -> httpHeaders.addAll(headers))
                    .bodyValue(input)
                    .retrieve()
                    .bodyToMono(CobResponse.class);
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }

    }
    public Mono<CobResponse> pegaCobTxid(String txId){
        try{
            WebClient webClient = WebClient.create(url);
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            headers.add("Authorization", "Bearer "
                    + auth.getToken("de744fca-a606-3b07-92e0-712cf31c9346",
                    "88c79394-6b3f-426d-86a2-1e74c922aaa7"));

            return webClient
                    .get()
                    .uri(url.concat("/"+txId))
                    .headers(httpHeaders -> httpHeaders.addAll(headers))
                    .retrieve()
                    .bodyToMono(CobResponse.class);
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }
    public Mono<?> pegaListaCob(String intervalo){
        try{
            WebClient webClient = WebClient.create(url);
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            headers.add("Authorization", "Bearer "
                    + auth.getToken("de744fca-a606-3b07-92e0-712cf31c9346",
                    "88c79394-6b3f-426d-86a2-1e74c922aaa7"));

            String newUrl = url.concat("?"+intervalo);
            return webClient
                    .get()
                    .uri(newUrl)
                    .headers(httpHeaders -> httpHeaders.addAll(headers))
                    .retrieve()
                    .bodyToMono(Object.class);

        }catch (Exception e){
            return null;
        }
    }
}
