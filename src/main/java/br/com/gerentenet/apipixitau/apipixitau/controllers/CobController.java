package br.com.gerentenet.apipixitau.apipixitau.controllers;

import br.com.gerentenet.apipixitau.apipixitau.model.CobInput;
import br.com.gerentenet.apipixitau.apipixitau.model.CobResponse;
import br.com.gerentenet.apipixitau.apipixitau.service.CobService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.reactive.function.client.WebClientResponseException;
import reactor.core.publisher.Mono;

@RestController
public class CobController {

    @Autowired
    private CobService serv;

    @PostMapping("/gera-cob")
    public ResponseEntity<?> geraCobrancaPix(@RequestBody CobInput input){
        try {
            Mono<CobResponse> responseMono = serv.geraCobPix(input);
            CobResponse response = responseMono.block();
            return ResponseEntity.status(HttpStatus.CREATED).body(response);
        }catch (WebClientResponseException e){
            HttpHeaders errorHeaders = new HttpHeaders();
            errorHeaders.setContentType(MediaType.APPLICATION_PROBLEM_JSON);
            return ResponseEntity.status(e.getStatusCode())
                    .headers(errorHeaders)
                    .body(e.getResponseBodyAsString());
        }
    }
    @PutMapping("/gera-cob-imediata/{txId}")
    public ResponseEntity<?> geraCobrancaImediata(@PathVariable String txId,
                                                  @RequestBody CobInput input){
        try{
            Mono<CobResponse> responseMono = serv.geraCobImediata(txId,input);
            CobResponse response = responseMono.block();
            return ResponseEntity.status(HttpStatus.CREATED).body(response);
        }catch (WebClientResponseException e){
            HttpHeaders errorHeaders = new HttpHeaders();
            errorHeaders.setContentType(MediaType.APPLICATION_PROBLEM_JSON);
            return ResponseEntity.status(e.getStatusCode())
                    .headers(errorHeaders)
                    .body(e.getResponseBodyAsString());
        }
    }
    @GetMapping("/pega-cob-txid/{txId}")
    public ResponseEntity<?> pegaCobrancaTxid(@PathVariable String txId){
        try{
            Mono<CobResponse> responseMono = serv.pegaCobTxid(txId);
            CobResponse response = responseMono.block();
            return ResponseEntity.ok().body(response);
        }catch (WebClientResponseException e){
            HttpHeaders errorHeaders = new HttpHeaders();
            errorHeaders.setContentType(MediaType.APPLICATION_PROBLEM_JSON);
            return ResponseEntity.status(e.getStatusCode())
                    .headers(errorHeaders)
                    .body(e.getResponseBodyAsString());
        }
    }
    @GetMapping("/list-cob/{intervalo}")
    public ResponseEntity<?> pegaListaCobranca(@PathVariable String intervalo){
        try{
            Mono<?> responseMono = serv.pegaListaCob(intervalo);
            return ResponseEntity.ok().body(responseMono.block());

        }catch (WebClientResponseException e){
            HttpHeaders errorHeaders = new HttpHeaders();
            errorHeaders.setContentType(MediaType.APPLICATION_PROBLEM_JSON);

            return ResponseEntity.status(e.getStatusCode())
                    .headers(errorHeaders)
                    .body(e.getResponseBodyAsString());
        }
    }
}
