package br.com.gerentenet.apipixitau.apipixitau.controllers;

import br.com.gerentenet.apipixitau.apipixitau.model.WebhookInput;
import br.com.gerentenet.apipixitau.apipixitau.service.WebhookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.reactive.function.client.WebClientResponseException;
import reactor.core.publisher.Mono;

@RestController
public class WebhookController {

    @Autowired
    private WebhookService webServ;

    @PutMapping("/webhook-config/{chave}")
    public ResponseEntity<?> webhookConfiguracao(@PathVariable String chave,
                                                 @RequestBody WebhookInput input){
        try {
            Mono<?> responseMono = webServ.webhookConfig(chave, input);
            return ResponseEntity.ok().body(responseMono);
        }catch (WebClientResponseException e){
            HttpHeaders errorHeadears = new HttpHeaders();
            errorHeadears.setContentType(MediaType.APPLICATION_PROBLEM_JSON);

            return ResponseEntity.status(e.getStatusCode())
                    .headers(errorHeadears)
                    .body(e.getResponseBodyAsString());
        }

    }
}
