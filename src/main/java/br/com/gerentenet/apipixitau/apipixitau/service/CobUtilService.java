package br.com.gerentenet.apipixitau.apipixitau.service;

import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
public class CobUtilService {

    public String criarApikey(){
        UUID uuid = UUID.randomUUID();
        String apiKey = uuid.toString();
        return apiKey;
    }
}
