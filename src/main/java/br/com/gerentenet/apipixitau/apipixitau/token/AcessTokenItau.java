package br.com.gerentenet.apipixitau.apipixitau.token;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
public class AcessTokenItau {
    private String access_token;
}
