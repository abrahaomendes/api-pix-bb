package br.com.gerentenet.apipixitau.apipixitau.service;

import br.com.gerentenet.apipixitau.apipixitau.model.WebhookInput;
import br.com.gerentenet.apipixitau.apipixitau.token.AuthTokenItau;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

@Service
public class WebhookService {

    @Autowired
    private AuthTokenItau auth;

    private String url = "https://sandbox.devportal.itau.com.br/itau-ep9-gtw-pix-recebimentos-ext-v2/v2/webhook";

    public Mono<?> webhookConfig(String chave, WebhookInput webhookUrl){
        try{
            WebClient webClient = WebClient.create(url);
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            headers.add("Authorization", "Bearer "
                    + auth.getToken("de744fca-a606-3b07-92e0-712cf31c9346",
                    "88c79394-6b3f-426d-86a2-1e74c922aaa7"));

            return webClient
                    .put()
                    .uri(url.concat("/"+chave))
                    .headers(httpHeaders -> httpHeaders.addAll(headers))
                    .bodyValue(webhookUrl)
                    .retrieve()
                    .bodyToMono(Object.class);
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }
}
