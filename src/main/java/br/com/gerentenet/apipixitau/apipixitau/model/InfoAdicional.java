package br.com.gerentenet.apipixitau.apipixitau.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
public class InfoAdicional {
    private String nome;
    private String valor;
}
