package br.com.gerentenet.apipixitau.apipixitau.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class CobResponse {
    private Calendario calendario;
    private String txid;
    private Integer revisao;
    private Devedor devedor;
    private String location;
    private String status;
    private Valor valor;
    private String pixCopiaECola;
    private String solicitacaoPagador;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private List<InfoAdicional>infoAdicionais;
}
