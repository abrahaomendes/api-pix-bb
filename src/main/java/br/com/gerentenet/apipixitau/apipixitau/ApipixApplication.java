package br.com.gerentenet.apipixitau.apipixitau;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApipixApplication {

    public static void main(String[] args) {
        SpringApplication.run(ApipixApplication.class, args);
    }

}
