package br.com.gerentenet.apipixitau.apipixitau.token;

import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

@Service
public class AuthTokenItau {

    public String getToken(String clientId, String clientSecret) {
        String urlToken = "https://sandbox.devportal.itau.com.br/api/oauth/jwt";
        WebClient webClient = WebClient.create(urlToken);

        MultiValueMap<String, String> bodyValues = new LinkedMultiValueMap<>();
        bodyValues.add("grant_type", "client_credentials");
        bodyValues.add("client_id", clientId);
        bodyValues.add("client_secret", clientSecret);

        Mono<AcessTokenItau> response = webClient.post()
                .uri(urlToken)
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_FORM_URLENCODED_VALUE)
                .bodyValue(bodyValues)
                .retrieve()
                .bodyToMono(AcessTokenItau.class);
        AcessTokenItau acess = response.block();

        return acess.getAccess_token();
    }

}
