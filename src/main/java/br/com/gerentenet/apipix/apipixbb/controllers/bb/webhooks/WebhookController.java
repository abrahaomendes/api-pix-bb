package br.com.gerentenet.apipix.apipixbb.controllers.bb.webhooks;


import br.com.gerentenet.apipix.apipixbb.domain.model.pix.Parametros;
import br.com.gerentenet.apipix.apipixbb.domain.model.webhook.WebhookResponse;
import br.com.gerentenet.apipix.apipixbb.domain.model.webhook.Webhook;
import br.com.gerentenet.apipix.apipixbb.domain.model.webhook.WebhookInput;
import br.com.gerentenet.apipix.apipixbb.protocolossl.ProtocoloConexaoSSLContexts;
import br.com.gerentenet.apipix.apipixbb.tokenpix.AuthToken;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.WebClientResponseException;
import reactor.core.publisher.Mono;


import java.net.URI;


@RestController
public class WebhookController {

    String certNomeComp = "C:\\INTERSYS CERTIFICADO 2024 SENHA 123456.pfx";
    String certSenha = "123456";

    private AuthToken acess = new AuthToken();

    private final ProtocoloConexaoSSLContexts sslContext = new ProtocoloConexaoSSLContexts();

    String token = acess.getToken("eyJpZCI6IjNkMjJkNDgtMiIsImNvZGlnb1B1YmxpY2Fkb3IiOjAsImNvZGlnb1NvZnR3YXJlIjo5ODg2OCwic2VxdWVuY2lhbEluc3RhbGFjYW8iOjF9",
            "eyJpZCI6Ijg4MWVkODQtZjhlZC00ZmVjLTllZjMtZGQ3NjI1M2U2ZTlkNWVkODU5YjgtODY4Yy0iLCJjb2RpZ29QdWJsaWNhZG9yIjowLCJjb2RpZ29Tb2Z0d2FyZSI6OTg4NjgsInNlcXVlbmNpYWxJbnN0YWxhY2FvIjoxLCJzZXF1ZW5jaWFsQ3JlZGVuY2lhbCI6MSwiYW1iaWVudGUiOiJob21vbG9nYWNhbyIsImlhdCI6MTcxNzUwOTMxMDI2OH0",
            "client_credentials",
            "webhook.write webhook.read");



    @PutMapping("/webhook/{chave}")
    public ResponseEntity<?> configWebhook(@PathVariable String chave, @RequestBody WebhookInput modelo) throws Exception {

        String urlWh = "https://api-pix.hm.bb.com.br/pix/v2/webhook/" + chave + "?gw-dev-app-key=14c1c37251edeab0df3d553edd40bed8";
        URI uri = URI.create(urlWh);


            WebClient webClient = sslContext.createWebClient(certNomeComp, certSenha);
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            headers.add("Authorization", "Bearer " + token);

        try {
            Mono<WebhookResponse> response = webClient.put()
                    .uri(uri)
                    .headers(httpHeaders -> httpHeaders.addAll(headers))
                    .bodyValue(modelo)
                    .retrieve()
                    .bodyToMono(WebhookResponse.class);
            WebhookResponse webresp = response.block();

            return ResponseEntity.ok().body(webresp);
        } catch (WebClientResponseException e) {
            System.err.println("Status code: " + e.getStatusCode());
            System.err.println("Response body: " + e.getResponseBodyAsString());

            HttpHeaders errorHeaders = new HttpHeaders();
            errorHeaders.setContentType(MediaType.APPLICATION_PROBLEM_JSON);

            return ResponseEntity.status(e.getStatusCode())
                    .headers(errorHeaders)
                    .body(e.getResponseBodyAsString());
        }
    }
    @GetMapping("/consulta-webhook/{intervalo}")
    public ResponseEntity<?> consultarWebhook(@PathVariable String intervalo) throws Exception {
        String urlCon = "https://api-pix.hm.bb.com.br/pix/v2/webhook?"+intervalo+"&gw-dev-app-key=14c1c37251edeab0df3d553edd40bed8";
        // exemplo : inicio=2024-06-25T00%3A00%3A00Z&fim=2024-06-28T00%3A00%3A00Z&paginacao.paginaAtual=0&paginacao.itensPorPagina=10
        URI uri = URI.create(urlCon);

            WebClient webClient = sslContext.createWebClient(certNomeComp, certSenha);
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            headers.add("Authorization", "Bearer " + token);

        try {
            Mono<?> response = webClient
                    .get()
                    .uri(uri)
                    .headers(httpHeaders -> httpHeaders.addAll(headers))
                    .retrieve()
                    .bodyToMono(Object.class);

            return ResponseEntity.ok().body(response.block());
        }catch (WebClientResponseException e){
            HttpHeaders errorHeaders = new HttpHeaders();
            errorHeaders.setContentType(MediaType.APPLICATION_PROBLEM_JSON);

            return ResponseEntity.status(e.getStatusCode())
                    .headers(errorHeaders)
                    .body(e.getResponseBodyAsString());
        }
    }
    @GetMapping("/consulta-webhook-chave/{chave}")
    public ResponseEntity<?> consultarWebhookChave(@PathVariable String chave) throws Exception {

        String urlWC = "https://api-pix.hm.bb.com.br/pix/v2/webhook/"+chave+"?gw-dev-app-key=14c1c37251edeab0df3d553edd40bed8";
        URI uri = URI.create(urlWC);

            WebClient webClient = sslContext.createWebClient(certNomeComp, certSenha);
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            headers.add("Authorization", "Bearer " + token);
        try {
            Mono<Webhook> response = webClient
                    .get()
                    .uri(uri)
                    .headers(httpHeaders -> httpHeaders.addAll(headers))
                    .retrieve()
                    .bodyToMono(Webhook.class);
            Webhook webResp = response.block();

            return ResponseEntity.ok().body(webResp);

        }catch (WebClientResponseException e){
            HttpHeaders errorHeaders = new HttpHeaders();
            errorHeaders.setContentType(MediaType.APPLICATION_PROBLEM_JSON);

            return ResponseEntity.status(e.getStatusCode())
                    .headers(errorHeaders)
                    .body(e.getResponseBodyAsString());
        }
    }
    @DeleteMapping("/delete-webhook/{chave}")
    public ResponseEntity<?> deletarWebhook(@PathVariable String chave) throws Exception {
        String urlDelW = "https://api-pix.hm.bb.com.br/pix/v2/webhook/"+chave+"?gw-dev-app-key=14c1c37251edeab0df3d553edd40bed8";
        URI uri = URI.create(urlDelW);


            WebClient webClient = sslContext.createWebClient(certNomeComp,certSenha);

            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            headers.add("Authorization", "Barear "+token);

        try{
            Mono<Void> response = webClient
                    .delete()
                    .uri(uri)
                    .headers(httpHeaders -> httpHeaders.addAll(headers))
                    .retrieve()
                    .bodyToMono(Void.class);


            return ResponseEntity.noContent().build();
        }catch (WebClientResponseException e){
            HttpHeaders errorHeaders = new HttpHeaders();
            errorHeaders.setContentType(MediaType.APPLICATION_PROBLEM_JSON);

            return ResponseEntity.status(e.getStatusCode())
                    .headers(errorHeaders)
                    .body(e.getResponseBodyAsString());
        }
    }

}



