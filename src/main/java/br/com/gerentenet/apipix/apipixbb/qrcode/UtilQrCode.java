package br.com.gerentenet.apipix.apipixbb.qrcode;

import java.awt.image.BufferedImage;

/**
 *
 * @author Alexandre
 */
public class UtilQrCode {
    
    /**
     *
     * @param QrCode passe o texto
     * @param gerarArquivo passe true ou false
     * @return BufferedImage
     */
    public static BufferedImage geraQrCode(String QrCode, Boolean gerarArquivo) {
        try {

            geradorQRCode QR = new geradorQRCode();
            QR.setMyCodeText(QrCode);
            return QR.geraArquivoQrCode(gerarArquivo);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
