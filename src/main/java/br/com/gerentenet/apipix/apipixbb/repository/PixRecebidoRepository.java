package br.com.gerentenet.apipix.apipixbb.repository;

import br.com.gerentenet.apipix.apipixbb.domain.model.entities.PixRecebido;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PixRecebidoRepository extends JpaRepository<PixRecebido, Long> {
}
