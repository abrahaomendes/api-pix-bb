package br.com.gerentenet.apipix.apipixbb.domain.model.pix;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Setter
@Getter
@NoArgsConstructor
public class CobImediataRev {
    private Calendario calendario;
    private Devedor devedor;
    private String status;
    private String valor;
    private String chave;
    private String solicitacaoPagador;
    private List<InfoAdicional>infoAdicionais;

}
