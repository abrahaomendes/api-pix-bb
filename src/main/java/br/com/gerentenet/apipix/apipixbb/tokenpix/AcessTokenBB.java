package br.com.gerentenet.apipix.apipixbb.tokenpix;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AcessTokenBB {
    private String access_token;
    private String type_token;
    private String expires_in;
}
