package br.com.gerentenet.apipix.apipixbb.domain.model.pix;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class PixParametrosResponse {
    @JsonProperty("parametros")
    private Parametros parametros;

    @JsonProperty("cobs")
    private List<PixInput> cobs;

    @JsonProperty("paginacao")
    private Paginacao paginacao;

}
