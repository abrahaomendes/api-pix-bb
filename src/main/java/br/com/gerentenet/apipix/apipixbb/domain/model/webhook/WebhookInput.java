package br.com.gerentenet.apipix.apipixbb.domain.model.webhook;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class WebhookInput {
    private String webhookUrl;
}
