package br.com.gerentenet.apipix.apipixbb.domain.model.webhook;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class WebhookResponse {
    private String type;
    private String title;
    private Integer status;
    private String detail;
    private String correlationId;
}
