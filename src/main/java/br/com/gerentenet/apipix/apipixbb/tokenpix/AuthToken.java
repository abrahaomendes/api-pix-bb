package br.com.gerentenet.apipix.apipixbb.tokenpix;


import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;


import java.util.Base64;
/**
 *
 * @author Abraão
 */
@Component
public class AuthToken {

    private final RestTemplate restTemplate = new RestTemplate();

    public String getToken(String clientId, String clientSecret, String grantType, String scope){

        String urlToken = "https://oauth.hm.bb.com.br/oauth/token";

        HttpHeaders headers = new HttpHeaders();

        byte[] auth = Base64.getEncoder().encode(clientId.concat(":").concat(clientSecret).getBytes());
        headers.set("Authorization", "Basic "+ new String(auth));
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        headers.set("Host", urlToken);
        MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
        map.add("grant_type", grantType);
        map.add("scope", scope);

        var request = new HttpEntity<>(map, headers);

       var acessToken = restTemplate.postForObject(urlToken,request, AcessTokenBB.class);

        return acessToken.getAccess_token();
    }
}
