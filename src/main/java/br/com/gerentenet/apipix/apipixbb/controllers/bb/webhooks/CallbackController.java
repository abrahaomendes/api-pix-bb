package br.com.gerentenet.apipix.apipixbb.controllers.bb.webhooks;

import br.com.gerentenet.apipix.apipixbb.domain.model.entities.PixRecebido;
import br.com.gerentenet.apipix.apipixbb.domain.model.webhook.Pix;
import br.com.gerentenet.apipix.apipixbb.domain.model.webhook.PixListWrapper;
import br.com.gerentenet.apipix.apipixbb.repository.PixRecebidoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class CallbackController {

    @Autowired
    private PixRecebidoRepository pixRepo;

    @PostMapping("/callback/pix")
    public ResponseEntity<?> handleCallback(@RequestBody PixListWrapper pixListWrapper) {

        List<Pix> pixList = pixListWrapper.getPix();
        for (Pix pix : pixList) {
            PixRecebido pixR = new PixRecebido();
            pixR.setEndToEndId(pix.getEndToEndId());
            pixR.setTxid(pix.getTxid());
            pixR.setValor(pix.getValor());
            pixR.setInfoPagador(pix.getInfoPagador());
            pixR.setHorario(pix.getHorario());
            pixRepo.save(pixR);
        }

        return new ResponseEntity<>(HttpStatus.OK);
    }
}
