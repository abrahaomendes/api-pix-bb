package br.com.gerentenet.apipix.apipixbb.controllers.bb;


import br.com.gerentenet.apipix.apipixbb.domain.model.pix.CobImediataRev;
import br.com.gerentenet.apipix.apipixbb.domain.model.pix.PixParametrosResponse;
import br.com.gerentenet.apipix.apipixbb.qrcode.UtilQrCode;
import br.com.gerentenet.apipix.apipixbb.domain.model.pix.PixInput;
import br.com.gerentenet.apipix.apipixbb.domain.model.pix.PixResponse;
import br.com.gerentenet.apipix.apipixbb.protocolossl.ProtocoloConexaoSSLContexts;
import br.com.gerentenet.apipix.apipixbb.tokenpix.AuthToken;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;

import org.springframework.web.bind.annotation.*;

import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.WebClientResponseException;
import reactor.core.publisher.Mono;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;

import java.io.ByteArrayOutputStream;
import java.net.URI;


@RestController
public class PixController {

    String certNomeComp = "C:\\INTERSYS CERTIFICADO 2024 SENHA 123456.pfx";
    String certSenha = "123456";

    @Autowired
    private AuthToken acess;
    @Autowired
    private ProtocoloConexaoSSLContexts sslContext;


    @PutMapping("/gera-cobranca-imediata/{txId}")
    public ResponseEntity<?> geraCobrancaPix(@RequestBody PixInput modelo, @PathVariable String txId) throws Exception {

        WebClient webClient = sslContext.createWebClient(certNomeComp, certSenha);

        String urlCob = "https://api-pix.hm.bb.com.br/pix/v2/cob/" + txId + "?gw-dev-app-key=14c1c37251edeab0df3d553edd40bed8";
        URI uri = URI.create(urlCob);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.add("Authorization", "Bearer " +  acess.getToken("eyJpZCI6IjQ0MzJiNDEtODAzYy00MTNmLTgzZDUtOSIsImNvZGlnb1B1YmxpY2Fkb3IiOjAsImNvZGlnb1NvZnR3YXJlIjo2ODUzNiwic2VxdWVuY2lhbEluc3RhbGFjYW8iOjF9",
                "eyJpZCI6IjQ4ZWU2MjU3LTliYjktNDEzMy05ZjJhLWYxM2M0Y2M2YzdhMyIsImNvZGlnb1B1YmxpY2Fkb3IiOjAsImNvZGlnb1NvZnR3YXJlIjo2ODUzNiwic2VxdWVuY2lhbEluc3RhbGFjYW8iOjEsInNlcXVlbmNpYWxDcmVkZW5jaWFsIjoxLCJhbWJpZW50ZSI6ImhvbW9sb2dhY2FvIiwiaWF0IjoxNjg5NzAzMDgxMDE4fQ",
                "client_credentials",
                "cob.write cob.read pix.write pix.read"));

        try {
            Mono<PixResponse> response = webClient.put()
                    .uri(uri)
                    .headers(httpHeaders -> httpHeaders.addAll(headers))
                    .bodyValue(modelo)
                    .retrieve()
                    .bodyToMono(PixResponse.class);

            PixResponse pixResponse = response.block();
            if (pixResponse != null && pixResponse.getPixCopiaECola() != null) {
                BufferedImage qrCodeImage = UtilQrCode.geraQrCode(pixResponse.getPixCopiaECola(), true);
                ByteArrayOutputStream caixa = new ByteArrayOutputStream();
                ImageIO.write(qrCodeImage, "png", caixa);
                byte[] qrCodeBytes = caixa.toByteArray();

                HttpHeaders imageHeaders = new HttpHeaders();
                imageHeaders.setContentType(MediaType.IMAGE_PNG);
                return new ResponseEntity<>(qrCodeBytes, imageHeaders, HttpStatus.OK);
            }
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        } catch (WebClientResponseException e) {

            HttpHeaders errorHeaders = new HttpHeaders();
            errorHeaders.setContentType(MediaType.APPLICATION_PROBLEM_JSON);

            return ResponseEntity.status(e.getStatusCode())
                    .headers(errorHeaders)
                    .body(e.getResponseBodyAsString());
        }
    }


    //COM QRCODE
    @PostMapping("/gera-cobranca")
    public ResponseEntity<?> geraCobrancaPix(@RequestBody PixInput modelo) throws Exception {
        WebClient webClient = sslContext.createWebClient(certNomeComp, certSenha);


        String urlCob = "https://api-pix.hm.bb.com.br/pix/v2/cob?gw-dev-app-key=14c1c37251edeab0df3d553edd40bed8";
        URI uri = URI.create(urlCob);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.add("Authorization", "Bearer " +  acess.getToken("eyJpZCI6IjQ0MzJiNDEtODAzYy00MTNmLTgzZDUtOSIsImNvZGlnb1B1YmxpY2Fkb3IiOjAsImNvZGlnb1NvZnR3YXJlIjo2ODUzNiwic2VxdWVuY2lhbEluc3RhbGFjYW8iOjF9",
                "eyJpZCI6IjQ4ZWU2MjU3LTliYjktNDEzMy05ZjJhLWYxM2M0Y2M2YzdhMyIsImNvZGlnb1B1YmxpY2Fkb3IiOjAsImNvZGlnb1NvZnR3YXJlIjo2ODUzNiwic2VxdWVuY2lhbEluc3RhbGFjYW8iOjEsInNlcXVlbmNpYWxDcmVkZW5jaWFsIjoxLCJhbWJpZW50ZSI6ImhvbW9sb2dhY2FvIiwiaWF0IjoxNjg5NzAzMDgxMDE4fQ",
                "client_credentials",
                "cob.write cob.read pix.write pix.read"));

        try{
        Mono<PixResponse> response = webClient.post()
                .uri(uri)
                .headers(httpHeaders -> httpHeaders.addAll(headers))
                .bodyValue(modelo)
                .retrieve()
                .bodyToMono(PixResponse.class);

            PixResponse pixResponse = response.block();
            if (pixResponse != null && pixResponse.getPixCopiaECola() != null) {
                BufferedImage qrCodeImage = UtilQrCode.geraQrCode(pixResponse.getPixCopiaECola(), true);
                ByteArrayOutputStream caixa = new ByteArrayOutputStream();
                ImageIO.write(qrCodeImage, "png", caixa);
                byte[] qrCodeBytes = caixa.toByteArray();

                HttpHeaders imageHeaders = new HttpHeaders();
                imageHeaders.setContentType(MediaType.IMAGE_PNG);
                return new ResponseEntity<>(qrCodeBytes, imageHeaders, HttpStatus.OK);
            }
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        } catch (WebClientResponseException e) {

            System.err.println("Status code: " + e.getStatusCode());
            System.err.println("Response body: " + e.getResponseBodyAsString());

            HttpHeaders errorHeaders = new HttpHeaders();
            errorHeaders.setContentType(MediaType.APPLICATION_PROBLEM_JSON);

            return ResponseEntity.status(e.getStatusCode())
                    .headers(errorHeaders)
                    .body(e.getResponseBodyAsString());
        }
    }
    @PostMapping("/gera-cobranca-sqr")
    public ResponseEntity<?> geraCobrancaPixSemQr(@RequestBody PixInput modelo) throws Exception {
        WebClient webClient = sslContext.createWebClient(certNomeComp, certSenha);


        String urlCob = "https://api-pix.hm.bb.com.br/pix/v2/cob?gw-dev-app-key=14c1c37251edeab0df3d553edd40bed8";
        URI uri = URI.create(urlCob);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.add("Authorization", "Bearer " +  acess.getToken("eyJpZCI6IjQ0MzJiNDEtODAzYy00MTNmLTgzZDUtOSIsImNvZGlnb1B1YmxpY2Fkb3IiOjAsImNvZGlnb1NvZnR3YXJlIjo2ODUzNiwic2VxdWVuY2lhbEluc3RhbGFjYW8iOjF9",
                "eyJpZCI6IjQ4ZWU2MjU3LTliYjktNDEzMy05ZjJhLWYxM2M0Y2M2YzdhMyIsImNvZGlnb1B1YmxpY2Fkb3IiOjAsImNvZGlnb1NvZnR3YXJlIjo2ODUzNiwic2VxdWVuY2lhbEluc3RhbGFjYW8iOjEsInNlcXVlbmNpYWxDcmVkZW5jaWFsIjoxLCJhbWJpZW50ZSI6ImhvbW9sb2dhY2FvIiwiaWF0IjoxNjg5NzAzMDgxMDE4fQ",
                "client_credentials",
                "cob.write cob.read pix.write pix.read"));

        try{
            Mono<PixResponse> response = webClient.post()
                    .uri(uri)
                    .headers(httpHeaders -> httpHeaders.addAll(headers))
                    .bodyValue(modelo)
                    .retrieve()
                    .bodyToMono(PixResponse.class);

            PixResponse pixResponse = response.block();

            return ResponseEntity.ok().body(pixResponse);
        } catch (WebClientResponseException e) {

            System.err.println("Status code: " + e.getStatusCode());
            System.err.println("Response body: " + e.getResponseBodyAsString());

            HttpHeaders errorHeaders = new HttpHeaders();
            errorHeaders.setContentType(MediaType.APPLICATION_PROBLEM_JSON);

            return ResponseEntity.status(e.getStatusCode())
                    .headers(errorHeaders)
                    .body(e.getResponseBodyAsString());
        }
    }


    @PatchMapping("/atualiza-cobranca/{txId}")
    public ResponseEntity<?> atualizaCobrancaPix(@PathVariable String txId, @RequestBody CobImediataRev modelo) throws Exception {
        WebClient webClient = sslContext.createWebClient(certNomeComp, certSenha);

        String urlCob = "https://api-pix.hm.bb.com.br/pix/v2/cob/"+txId+"?gw-dev-app-key=14c1c37251edeab0df3d553edd40bed8";
        URI uri = URI.create(urlCob);
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            headers.add("Authorization", "Bearer " +  acess.getToken("eyJpZCI6IjQ0MzJiNDEtODAzYy00MTNmLTgzZDUtOSIsImNvZGlnb1B1YmxpY2Fkb3IiOjAsImNvZGlnb1NvZnR3YXJlIjo2ODUzNiwic2VxdWVuY2lhbEluc3RhbGFjYW8iOjF9",
                    "eyJpZCI6IjQ4ZWU2MjU3LTliYjktNDEzMy05ZjJhLWYxM2M0Y2M2YzdhMyIsImNvZGlnb1B1YmxpY2Fkb3IiOjAsImNvZGlnb1NvZnR3YXJlIjo2ODUzNiwic2VxdWVuY2lhbEluc3RhbGFjYW8iOjEsInNlcXVlbmNpYWxDcmVkZW5jaWFsIjoxLCJhbWJpZW50ZSI6ImhvbW9sb2dhY2FvIiwiaWF0IjoxNjg5NzAzMDgxMDE4fQ",
                    "client_credentials",
                    "cob.write cob.read pix.write pix.read"));

            Mono<PixResponse> response = webClient.patch()
                    .uri(uri)
                    .accept(MediaType.APPLICATION_JSON)
                    .headers(httpHeaders -> httpHeaders.addAll(headers))
                    .bodyValue(modelo)
                    .retrieve()
                    .bodyToMono(PixResponse.class);
            PixResponse response1 = response.block();
            return ResponseEntity.ok().body(response1);
        }catch (WebClientResponseException e) {
            HttpHeaders errorHeaders = new HttpHeaders();
            errorHeaders.setContentType(MediaType.APPLICATION_PROBLEM_JSON);

            return ResponseEntity.status(e.getStatusCode())
                    .headers(errorHeaders)
                    .body(e.getResponseBodyAsString());
        }
    }


    @GetMapping("/lista-pix/{intervalo}")
    public ResponseEntity<?> geraListaPix(@PathVariable String intervalo) throws Exception {
        String urlCob = "https://api-pix.hm.bb.com.br/pix/v2/cob?".concat(intervalo).concat("&gw-dev-app-key=14c1c37251edeab0df3d553edd40bed8");
        // intervaloExemplo="inicio=2024-06-15T00:00:00Z&fim=2024-06-19T23:59:59Z&cnpj=12345678000195&locationPresente=f&status=active&paginacao.paginaAtual=0&paginacao.itensPorPagina=100;
        URI uri = URI.create(urlCob);
        WebClient webClient = sslContext.createWebClient(certNomeComp, certSenha);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.add("Authorization", "Bearer " +  acess.getToken("eyJpZCI6IjQ0MzJiNDEtODAzYy00MTNmLTgzZDUtOSIsImNvZGlnb1B1YmxpY2Fkb3IiOjAsImNvZGlnb1NvZnR3YXJlIjo2ODUzNiwic2VxdWVuY2lhbEluc3RhbGFjYW8iOjF9",
                "eyJpZCI6IjQ4ZWU2MjU3LTliYjktNDEzMy05ZjJhLWYxM2M0Y2M2YzdhMyIsImNvZGlnb1B1YmxpY2Fkb3IiOjAsImNvZGlnb1NvZnR3YXJlIjo2ODUzNiwic2VxdWVuY2lhbEluc3RhbGFjYW8iOjEsInNlcXVlbmNpYWxDcmVkZW5jaWFsIjoxLCJhbWJpZW50ZSI6ImhvbW9sb2dhY2FvIiwiaWF0IjoxNjg5NzAzMDgxMDE4fQ",
                "client_credentials",
                "cob.write cob.read pix.write pix.read"));

        try {
            Mono<PixParametrosResponse> response = webClient.get()
                    .uri(uri)
                    .headers(httpHeaders -> httpHeaders.addAll(headers))
                    .retrieve()
                    .bodyToMono(PixParametrosResponse.class);

            PixParametrosResponse params = response.block();

            return ResponseEntity.ok().body(params);
        }catch (WebClientResponseException e){
            HttpHeaders errorHeaders = new HttpHeaders();
            errorHeaders.setContentType(MediaType.APPLICATION_PROBLEM_JSON);

            return ResponseEntity.status(e.getStatusCode())
                    .headers(errorHeaders)
                    .body(e.getResponseBodyAsString());
        }
    }

    @GetMapping("/pega-pix/{txId}")
    public ResponseEntity<?> pegaPixId(@PathVariable String txId) throws Exception {
        String urlCob = "https://api-pix.hm.bb.com.br/pix/v2/cob/"+ txId +"?gw-dev-app-key=14c1c37251edeab0df3d553edd40bed8";
        URI uri = URI.create(urlCob);
        WebClient webClient = sslContext.createWebClient(certNomeComp, certSenha);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.add("Authorization", "Bearer " +  acess.getToken("eyJpZCI6IjQ0MzJiNDEtODAzYy00MTNmLTgzZDUtOSIsImNvZGlnb1B1YmxpY2Fkb3IiOjAsImNvZGlnb1NvZnR3YXJlIjo2ODUzNiwic2VxdWVuY2lhbEluc3RhbGFjYW8iOjF9",
                "eyJpZCI6IjQ4ZWU2MjU3LTliYjktNDEzMy05ZjJhLWYxM2M0Y2M2YzdhMyIsImNvZGlnb1B1YmxpY2Fkb3IiOjAsImNvZGlnb1NvZnR3YXJlIjo2ODUzNiwic2VxdWVuY2lhbEluc3RhbGFjYW8iOjEsInNlcXVlbmNpYWxDcmVkZW5jaWFsIjoxLCJhbWJpZW50ZSI6ImhvbW9sb2dhY2FvIiwiaWF0IjoxNjg5NzAzMDgxMDE4fQ",
                "client_credentials",
                "cob.write cob.read pix.write pix.read"));

        try {
            Mono<PixResponse> response = webClient.get()
                    .uri(uri)
                    .headers(httpHeaders -> httpHeaders.addAll(headers))
                    .retrieve()
                    .bodyToMono(PixResponse.class);

            PixResponse params = response.block();

            return ResponseEntity.ok().body(params);
        }catch (WebClientResponseException e){
            HttpHeaders errorHeadrs = new HttpHeaders();
            errorHeadrs.setContentType(MediaType.APPLICATION_PROBLEM_JSON);

            return ResponseEntity.status(e.getStatusCode())
                    .headers(errorHeadrs)
                    .body(e.getResponseBodyAsString());
        }
    }
}

