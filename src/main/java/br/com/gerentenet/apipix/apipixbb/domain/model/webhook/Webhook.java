package br.com.gerentenet.apipix.apipixbb.domain.model.webhook;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
public class Webhook {

    private String chave;
    private String criacao;
    private String webhookUrl;
}
