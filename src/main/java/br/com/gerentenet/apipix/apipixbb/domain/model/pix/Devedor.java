package br.com.gerentenet.apipix.apipixbb.domain.model.pix;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
public class Devedor {
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String logradouro;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String cidade;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String uf;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String cep;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String cpf;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String cnpj;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String nome;

}
