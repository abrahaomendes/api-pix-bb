package br.com.gerentenet.apipix.apipixbb.domain.model.pix;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class InfoAdicional {
    private String nome;
    private String valor;
}
