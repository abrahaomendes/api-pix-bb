package br.com.gerentenet.apipix.apipixbb.protocolossl;

import io.netty.handler.ssl.SslContext;
import io.netty.handler.ssl.SslContextBuilder;
import io.netty.resolver.DefaultAddressResolverGroup;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.client.reactive.ReactorClientHttpConnector;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.netty.http.client.HttpClient;

import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.TrustManagerFactory;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.security.KeyStore;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;

@Component
public class ProtocoloConexaoSSLContexts {

    private static final String[] certs = {
            "C:\\Apos 05-01-2024\\DigiCert_Global_Root_G2.cer",
            "C:\\Apos 05-01-2024\\GeoTrust_EV_RSA_CA_G2.cer",
            "C:\\Apos 05-01-2024\\api.webhook.hm.bb.com.br.cer"
    };

    private static final String trustStorePath = System.getProperty("java.io.tmpdir") + System.getProperty("file.separator") + "test.keystore";
    private static final char[] password = "123456".toCharArray();

    static {
        System.setProperty("jdk.tls.maxHandshakeMessageSize", "60000");
    }

    public WebClient createWebClient(String certPath, String certPassword) throws Exception {
        KeyStore keyStore = KeyStore.getInstance("PKCS12");
        keyStore.load(new FileInputStream(certPath), certPassword.toCharArray());

        KeyManagerFactory keyManagerFactory = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
        keyManagerFactory.init(keyStore, certPassword.toCharArray());

        KeyStore trustStore = getKeyStoreWithCustomCerts();

        TrustManagerFactory trustManagerFactory = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
        trustManagerFactory.init(trustStore);


        SslContext sslContext = SslContextBuilder.forClient()
                .keyManager(keyManagerFactory)
                .trustManager(trustManagerFactory)
                .build();

        HttpClient httpClient = HttpClient.create()
                .secure(sslSpec -> sslSpec.sslContext(sslContext))
                .resolver(DefaultAddressResolverGroup.INSTANCE);


        return WebClient.builder()
                .clientConnector(new ReactorClientHttpConnector(httpClient))
                .baseUrl("https://api-pix.hm.bb.com.br")
                .defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .defaultHeader(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE)
                .build();

    }

    private KeyStore getKeyStoreWithCustomCerts() throws Exception {
        KeyStore keyStore = KeyStore.getInstance(KeyStore.getDefaultType());
        keyStore.load(null, null);
        for (String certFile : certs) {
            try (FileInputStream fileInputStream = new FileInputStream(certFile)) {
                X509Certificate certificate = (X509Certificate) CertificateFactory.getInstance("X.509").generateCertificate(fileInputStream);
                keyStore.setCertificateEntry(certFile, certificate);
            }
        }

        keyStore.store(new FileOutputStream(trustStorePath), password);
        return keyStore;
    }
}