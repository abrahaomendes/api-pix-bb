package br.com.gerentenet.apipix.apipixbb.domain.model.pix;


import br.com.gerentenet.apipix.apipixbb.domain.model.webhook.Webhook;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class Parametros {
    @JsonProperty("inicio")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss'Z'", timezone = "UTC")
    private String inicio;

    @JsonProperty("fim")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss'Z'", timezone = "UTC")
    private String fim;

    @JsonProperty("paginacao")
    private Paginacao paginacao;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("webhooks")
    private List<Webhook> webhooks;
}
