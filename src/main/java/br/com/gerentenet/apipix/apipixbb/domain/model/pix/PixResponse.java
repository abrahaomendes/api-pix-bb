package br.com.gerentenet.apipix.apipixbb.domain.model.pix;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class PixResponse {

    private Calendario calendario;
    private String txid;
    private Integer revisao;
    private Devedor devedor;
    private String status;
    private Valor valor;
    private String chave;
    private String solicitacaoPagador;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private List<InfoAdicional> infoAdicionais;
    private String pixCopiaECola;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("pix")
    private List<Pix> pix;


}
