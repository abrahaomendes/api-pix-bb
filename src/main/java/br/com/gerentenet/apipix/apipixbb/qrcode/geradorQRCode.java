package br.com.gerentenet.apipix.apipixbb.qrcode;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.EnumMap;
import java.util.Map;

public class geradorQRCode {
    
    private String myCodeText = "https://crunchify.com/";
    private String filePath = "teste_qrcode";
    private String fileType = "png";
    
    public BufferedImage geraArquivoQrCode(Boolean gerarArquivo){

            int size = 200;
            
            File myFile = new File(filePath+"."+fileType);
            try {

                    Map<EncodeHintType, Object> hintMap = new EnumMap<EncodeHintType, Object>(EncodeHintType.class);
                    hintMap.put(EncodeHintType.CHARACTER_SET, "UTF-8");

                    // Now with zxing version 3.2.1 you could change border size (white border size to just 1)
                    hintMap.put(EncodeHintType.MARGIN, 1); /* default = 4 */
                    hintMap.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.L);

                    QRCodeWriter qrCodeWriter = new QRCodeWriter();
                    BitMatrix byteMatrix = qrCodeWriter.encode(myCodeText, BarcodeFormat.QR_CODE, size,
                                    size, hintMap);
                    int CrunchifyWidth = byteMatrix.getWidth();
                    BufferedImage image = new BufferedImage(CrunchifyWidth, CrunchifyWidth,
                                    BufferedImage.TYPE_INT_RGB);
                    
                    //gera arquivo?
                    if(gerarArquivo){
                    
                        image.createGraphics();

                        Graphics2D graphics = (Graphics2D) image.getGraphics();
                        graphics.setColor(Color.WHITE);
                        graphics.fillRect(0, 0, CrunchifyWidth, CrunchifyWidth);
                        graphics.setColor(Color.BLACK);

                        for (int i = 0; i < CrunchifyWidth; i++) {
                                for (int j = 0; j < CrunchifyWidth; j++) {
                                        if (byteMatrix.get(i, j)) {
                                                graphics.fillRect(i, j, 1, 1);
                                        }
                                }
                        }
                        ImageIO.write(image, fileType, myFile);
                    }
                    return image;
            } catch (WriterException | IOException e) {                
                e.printStackTrace();
            }
            System.out.println("QR Code Gerado com sucesso!.");
            return null;
    }

    public String getMyCodeText() {
        return myCodeText;
    }

    public void setMyCodeText(String myCodeText) {
        this.myCodeText = myCodeText;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }
    
}
