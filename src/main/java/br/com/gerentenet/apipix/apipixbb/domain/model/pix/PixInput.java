package br.com.gerentenet.apipix.apipixbb.domain.model.pix;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

/**
 *
 * @author Abraão
 */
@Setter
@Getter
@NoArgsConstructor
public class PixInput {

    private Calendario calendario;
    @JsonProperty("devedor")
    private Devedor devedor;
    private Valor valor;
    private String chave;

    @JsonProperty("solicitacaoPagador")
    private String solicitacaoPagador;

    @JsonProperty("infoAdicionais")
    private List<InfoAdicional> infoAdicionais;


}
