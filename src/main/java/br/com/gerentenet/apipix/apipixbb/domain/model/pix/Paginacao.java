package br.com.gerentenet.apipix.apipixbb.domain.model.pix;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class Paginacao {

    @JsonProperty("paginaAtual")
    private Integer paginaAtual;

    @JsonProperty("itensPorPagina")
    private Integer itensPorPagina;

    @JsonProperty("quantidadeDePaginas")
    private Integer quantidadeDePaginas;

    @JsonProperty("quantidadeTotalDeItens")
    private Integer quantidadeTotalDeItens;
}
