package br.com.gerentenet.apipix.apipixbb.domain.model.webhook;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class PixListWrapper {
    private List<Pix> pix;
}
