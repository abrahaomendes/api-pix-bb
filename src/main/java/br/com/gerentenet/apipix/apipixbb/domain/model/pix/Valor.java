package br.com.gerentenet.apipix.apipixbb.domain.model.pix;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Valor {
    private String original;
}
