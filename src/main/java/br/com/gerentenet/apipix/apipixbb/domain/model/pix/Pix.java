package br.com.gerentenet.apipix.apipixbb.domain.model.pix;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
public class Pix {
    private String endToEndId;
    private String txid;
    private String valor;
    private String horario;
    private String infoPagador;

}
